#ifndef EXEC_RESULTS_H
#define EXEC_RESULTS_H

#include <vector>
#include <string>

struct ExecResults {
    std::string m_solver_name;
    std::vector<int> m_clusters;
    double m_exec_time;
};

#endif // EXEC_RESULTS_H