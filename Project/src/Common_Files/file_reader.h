#ifndef FILE_READER_H
#define FILE_READER_H

#include "coordinate.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

class FileReader {
public:
    static std::vector<Coordinate> read(const std::string & file_name);
};

#endif // FILE_READER_H
