#include "file_reader.h"

std::vector<Coordinate> FileReader::read(const std::string & file_name) {
    std::vector<Coordinate> vec_coordinates;
    std::fstream file;
    std::string str_line;
    int counter_line = 0;
    int prev_num, cur_num;

    file.open(file_name, std::ios::in);
    if(!file.is_open()) {
        std::cout << "File " << file_name << " could not be opened" << std::endl;
        return vec_coordinates;
    }

    while(std::getline(file, str_line)) {
        // read current number
        cur_num = std::stoi(str_line);

        // insert coordinates(two consecutive numbers into the vector of coordinates)
        if(counter_line % 2 == 1)
            vec_coordinates.push_back(Coordinate{prev_num, cur_num});
        
        prev_num = cur_num;
        counter_line++;
    }

    file.close();

    return vec_coordinates;
}
