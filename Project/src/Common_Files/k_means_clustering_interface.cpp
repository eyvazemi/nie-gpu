#include "k_means_clustering_interface.h"
#include "exec_results.h"

KMeansClusteringInterface::KMeansClusteringInterface(
    const std::string & solver_name,
    const std::vector<Coordinate> & vec_coordinates
) : m_vec_coordinates(vec_coordinates) 
{
    m_exec_results = new ExecResults;
    m_exec_results->m_solver_name = solver_name;
}

KMeansClusteringInterface::~KMeansClusteringInterface() {
    delete m_exec_results;
}

const ExecResults * KMeansClusteringInterface::run() {
    // run pre calculation stage
    pre_calculation_stage();

    // start measuring time
    std::chrono::high_resolution_clock::time_point time_start = std::chrono::high_resolution_clock::now();
    
    // run main clustering algoritm
    run_clustering();

    // stop measuring time
    std::chrono::high_resolution_clock::time_point time_end = std::chrono::high_resolution_clock::now();

    // calculate elapsed time
    m_exec_results->m_exec_time = (std::chrono::duration_cast<std::chrono::milliseconds>(time_end - time_start)).count();

    // run post calculation stage
    post_calculation_stage();

    return m_exec_results;
}
