#ifndef K_MEANS_CLUSTERING_INTERFACE_H
#define K_MEANS_CLUSTERING_INTERFACE_H

#include "exec_results.h"
#include "coordinate.h"

#include <vector>
#include <string>
#include <chrono>

class KMeansClusteringInterface {
public:
    KMeansClusteringInterface(const std::string & solver_name, const std::vector<Coordinate> & vec_coordinates);
    virtual ~KMeansClusteringInterface();
    const ExecResults * run();

private: // methods
    virtual void pre_calculation_stage() = 0;
    virtual void run_clustering() = 0;
    virtual void post_calculation_stage() = 0;

private: // members
    ExecResults * m_exec_results;
    const std::vector<Coordinate> & m_vec_coordinates;
};

#endif // K_MEANS_CLUSTERING_INTERFACE_H
