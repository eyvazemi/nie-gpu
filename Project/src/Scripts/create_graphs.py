import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import sys
from os.path import exists

# number of text fitting on the graph stacked on top of each other
# used for determining space between text on plot
val_count = 38

# count of spaces by which text on plot will be descended
val_space_init_count = 90

# solvers that should be excluded from the graph
csv_headers_exclude = []

def plot_graph(values_type, x_values, y_values, image_file_name, precision):
    # give size to the figure
    plt.figure(figsize = (10, 6))

    # set title to the plot
    plt.title("Matrices height and width range: " + str(int(x_values[0])) + " - " +
        str(int(x_values[len(x_values) - 1])) + "\nValues type: " + values_type + "\nPrecision: " + str(precision)
    )

    # set labels to the axes
    plt.xlabel("Matrix dimension")
    plt.ylabel("Time of execution(in milliseconds)")

    # create proper spacing for plot text
    y_values_spaced = {}

    # maximum value between all solvers
    max_val = -1

    for i, solver_name in enumerate(y_values.keys()):
        index = len(y_values[solver_name]) - 1
        y_values_spaced[solver_name] = y_values[solver_name][index]

        if y_values_spaced[solver_name] > max_val:
            max_val = y_values_spaced[solver_name]

    # sort dictionary by value
    y_values_spaced = {key: value for key, value in sorted(y_values_spaced.items(), key = lambda item: item[1])}
    
    # find value for given solver that is smaller but closest to this solver's value
    for i, solver_name in enumerate(y_values_spaced.keys()):
        closest_minimum = -1 
        for j, other_solver_name in enumerate(y_values_spaced.keys()):
            if (
                    other_solver_name != solver_name and
                    y_values_spaced[other_solver_name] < y_values_spaced[solver_name] and
                    y_values_spaced[other_solver_name] > closest_minimum
                ):
                closest_minimum = y_values_spaced[other_solver_name]

        # change spacing for between this solver and solver below
        y_values_spaced[solver_name] -= max_val / val_space_init_count

        if closest_minimum != -1 and abs(y_values_spaced[solver_name] - closest_minimum) < max_val / val_count:
            y_values_spaced[solver_name] = closest_minimum + max_val / val_count

    # instantiate graph
    for i, solver_name in enumerate(y_values.keys()):
        plt.plot(x_values, y_values[solver_name], label = solver_name, marker = 'o')

        for j, value in enumerate(y_values[solver_name]):
            if j == len(y_values[solver_name]) - 1:
                plt.text(x_values[len(x_values) - 1] + 20, y_values_spaced[solver_name], str("{:.6f}".format(value)))

    # show legend
    plt.legend()

    # turn scientific notation off
    ax = plt.gca()
    ax.ticklabel_format(useOffset = False, style = 'plain', axis = 'x')
    ax.ticklabel_format(useOffset = False, style = 'plain', axis = 'y')

    # save the graph as an image
    plt.savefig(image_file_name)


def generate_graph(file_name, values_type, tests_type, graphs_dir, precision):
    # x and y values for the graph
    x_values = []
    y_values = {}

    image_file_name = graphs_dir + "execution_time_" + \
        values_type + "_" + tests_type + "_" + precision + ".png"

    # read csv file into data frame
    if not exists(file_name):
        return

    df = pd.read_csv(file_name)

    # names of the csv file headers
    csv_headers = list(df.columns.values)

    # read csv values into dictionary
    for solver_name in csv_headers[1:]:
        # do not include this solver if it is given as excluded in arguments
        if (solver_name in set(csv_headers_exclude)) == False:
            y_values[solver_name] = []

    # process the rows one by one
    for i, row in df.iterrows():
        x_values.append(row[0])
        for (solver_name, solver_val) in zip(csv_headers[1:], row[1:]):
            # do not include this solver if it is given as excluded in arguments
            if (solver_name in set(csv_headers_exclude)) == False:
                y_values[solver_name].append(solver_val)

    # create plot
    plot_graph(values_type, x_values, y_values, image_file_name, precision)

# get csv headers to be excluded
for csv_header in sys.argv[6:]:
    csv_headers_exclude.append(csv_header)

# generate a graph with given argument
generate_graph(sys.argv[2], sys.argv[3], sys.argv[5], sys.argv[1], sys.argv[4])
