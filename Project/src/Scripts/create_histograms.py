import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import sys
from os.path import exists
from matplotlib.ticker import PercentFormatter

def create_histogram(dict_solvers_correctness, x_values, values_type, image_file_name, precision):
    # give size to the figure
    plt.figure(figsize = (10, 6))

    # set title to the plot
    plt.title("Matrices height and width range: " + str(int(x_values[0])) + " - " +
        str(int(x_values[len(x_values) - 1])) + "\nValues type: " + values_type + "\nPrecision: " + str(precision)
    )

    # set labels to the axes
    plt.xlabel("Solvers (with matrix heights and widths ranging from " + str(x_values[0]) + " to " + str(x_values[len(x_values) - 1]) + ")")
    plt.ylabel("Correctness of solution on average(in percentage)")

    # show text on bar charts
    count = 1
    for solver_name in dict_solvers_correctness.keys():
        solver_correctness = dict_solvers_correctness[solver_name]
        plt.bar(count, solver_correctness, label = solver_name)
        plt.text(count - 0.25, solver_correctness + 0.01, str("{:.3f}".format(solver_correctness)) + "%")
        count += 1

    # show legend
    plt.legend(bbox_to_anchor=(0, -0.1, 1, 0), loc = "upper left", mode = "expand", ncol=2)

    # set percentage for y-axis
    #plt.gca().yaxis.set_major_formatter(PercentFormatter(1))

    # save the histogram as an image
    plt.savefig(image_file_name, bbox_inches='tight')


def generate_histogram(file_name, values_type, tests_type, graphs_dir, precision):
    # dictionary with solvers
    dict_solvers_correctness = {}
    x_values = []

    image_file_name = graphs_dir + "execution_correctness_" + \
        values_type + "_" + tests_type + "_" + precision + ".png"

    # read csv file into data frame
    if not exists(file_name):
        return
    
    df = pd.read_csv(file_name)

    # names of the csv file headers
    csv_headers = list(df.columns.values)

    for solver_name in csv_headers[1:]:
        dict_solvers_correctness[solver_name] = 0

    # process the rows one by one
    for i, row in df.iterrows():
        x_values.append("{:.0f}".format(row[0]))
        for (solver_name, solver_correctness) in zip(csv_headers[1:], row[1:]):
            dict_solvers_correctness[solver_name] += solver_correctness

    # find average correctness of every solver
    for solver_name in dict_solvers_correctness.keys():
        dict_solvers_correctness[solver_name] = dict_solvers_correctness[solver_name] / len(df.index)

    # create histogram and save it as an image file
    create_histogram(dict_solvers_correctness, x_values, values_type, image_file_name, precision)

# generate a histogram with given arguments
generate_histogram(sys.argv[2], sys.argv[3], sys.argv[5], sys.argv[1], sys.argv[4])
