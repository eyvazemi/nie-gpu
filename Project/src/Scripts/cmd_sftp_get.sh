#!/bin/sh

if [ "$#" -ne 3 ]
then
    echo "Incorrect number of arguments, usage"
    echo "cmd_sftp.sh <user-name> <server-name> <remote-directory-to-be-download>"
    exit 1
fi

USER="$1"
SERVER="$2"
REMOTE_DIR="$3"

GET_COMMAND="get -r $REMOTE_DIR"$'\n'

sftp "${USER}${SERVER}" <<EOF
$GET_COMMAND
quit
EOF
