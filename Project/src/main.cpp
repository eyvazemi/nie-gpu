#include "./Common_Files/coordinate.h"
#include "./Common_Files/file_reader.h"

#include <iostream>
#include <string>
#include <vector>

int main(int argc, char ** argv) {
    // input contains sample file name and value of K
    if(argc != 3) {
        std::cout << "Incorrect input: file name and value of K must be provided." << std::endl;
        return 1;
    }

    // read input
    int k_value = std::stoi(argv[2]);
    std::vector<Coordinate> vec_coordinates = FileReader::read(std::string(argv[1]));

    // print first 10 coordinates(for testing)
    for(int i = 0; i < 10; i++)
        std::cout << vec_coordinates[i].m_x << ", " << vec_coordinates[i].m_y << std::endl;

    return 0;
}
