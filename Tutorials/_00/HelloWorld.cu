#include<stdlib.h>
#include<stdio.h>
#include<math.h>


// definitions of kernels
__global__ void HelloWorld() // must be void
{
  //executed on GPU
  printf("Hello World!\n");
}


__global__ void HelloWorld2()
{
  //identification of the thread
  int i = threadIdx.x;  // index inside block
  printf("Hello World from thread %i !\n",i);
}


__global__ void HelloWorld3()
{
  //identification of the thread
  int i = threadIdx.x;  // index inside block
  int j = blockIdx.x;  // index of block
  printf("Hello World from thread %i in block %i!\n",i,j);
}

int main()
{
  // kernel call
  HelloWorld3<<<3, 4>>>();
}