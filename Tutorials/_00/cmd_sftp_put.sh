#!/bin/sh

if [ "$#" -ne 3 ]
then
    echo "Incorrect number of arguments, usage"
    echo "cmd_sftp.sh <user-name> <server-name> <destination-directory>"
    exit 1
fi

USER="$1"
SERVER="$2"
DESTINATION_DIR="$3"

PUT_COMMAND="put -r . $DESTINATION_DIR"$'\n'

sftp "${USER}${SERVER}" <<EOF
$PUT_COMMAND
quit
EOF
